<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Ari Wahyuni-Profil</title>
</head>
<body>
   <!-- Header Starts -->
    <header class="header" id="navbar-collapse-toggle">
    <!-- Fixed Navigation Starts -->
    <ul class="icon-menu d-none d-lg-block revealator-slideup revealator-once revealator-delay1">
        @if ($page==1)
        <li class="icon-box active">
            @else
            <li class='icon-box'>
                @endif
            <i class="fa fa-home"></i>
            <a href="{{route('home')}}">
                <h2>Home</h2>
            </a>
        </li>
        @if ($page==2)
        <li class="icon-box active">
            @else
            <li class='icon-box'>
                @endif
            <i class="fa fa-user"></i>
            <a href="{{route('about')}}">
                <h2>About</h2>
            </a>
        </li>
        @if ($page==3)
        <li class="icon-box active">
            @else
            <li class='icon-box'>
                @endif
            <i class="fa fa-briefcase"></i>
            <a href="{{route('portfolio')}}">
                <h2>Portfolio</h2>
            </a>
        </li>
        @if ($page==4)
        <li class="icon-box active">
            @else
            <li class='icon-box'>
                @endif
            <i class="fa fa-envelope-open"></i>
            <a href="{{route('blog')}}">
                <h2>Blog</h2>
            </a>
        </li>
        @if ($page==5)
        <li class="icon-box active">
            @else
            <li class='icon-box'>
                @endif
            <i class="fa fa-comments"></i>
            <a href="{{route('contact')}}">
                <h2>Contact</h2>
            </a>
        </li>
    </ul>
    <!-- Fixed Navigation Ends -->

    <!-- Mobile Menu Starts -->
    <nav role="navigation" class="d-block d-lg-none">
        <div id="menuToggle">
            <input type="checkbox" />
            <span></span>
            <span></span>
            <span></span>
            <ul class="list-unstyled" id="menu">
                <li class="active"><a href="index.html"><i class="fa fa-home"></i><span>Home</span></a></li>
                <li><a href="about.html"><i class="fa fa-user"></i><span>About</span></a></li>
                <li><a href="portfolio.html"><i class="fa fa-folder-open"></i><span>Portfolio</span></a></li>
                <li><a href="contact.html"><i class="fa fa-envelope-open"></i><span>Contact</span></a></li>
                <li><a href="blog.html"><i class="fa fa-comments"></i><span>Blog</span></a></li>
            </ul>
        </div>
    </nav>
    <!-- Mobile Menu Ends -->
</header>
<!-- Header Ends -->
@yield('layouts.app')
</body>
</html>